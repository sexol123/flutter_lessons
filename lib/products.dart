import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Products extends StatelessWidget {
  final List<Map<String, dynamic>> products;

  Products(this.products);

  Widget _buildItem(BuildContext context, int index) => Card(
        child: Column(
          children: <Widget>[
            Image.asset(products[index]['image']),
            Container(
              padding: EdgeInsets.only(top: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    products[index]['title'],
                    style: TextStyle(
                        fontSize: 26.0,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'GreatVibes'),
                  ),
                  SizedBox(width: 8.0,),
                  Text(products[index]['price'].toString()),
                ], 
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                    child: Text('Deatails'),
                    onPressed: () => Navigator.pushNamed<bool>(
                          context,
                          '/product/' + index.toString(),
                        )),
              ],
            )
          ],
        ),
      );

  Widget _buildProductCard() {
    return products.length > 0
        ? ListView.builder(
            itemBuilder: _buildItem,
            itemCount: products.length,
          )
        : Center(
            child: Text("Empty list of products..."),
          );
  }

  @override
  Widget build(BuildContext context) => _buildProductCard();
}

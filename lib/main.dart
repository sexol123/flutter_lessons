import 'package:flutter/material.dart';
import 'package:flutter_course/pages/product_page.dart';

import 'pages/products_admin_page.dart';
import 'pages/products_page.dart';

main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  List<Map<String, dynamic>> _products = [];

  void _addProduct(Map<String, dynamic > product) {
    setState(() {
      _products.add(product);
    });
  }

  void _deleteProduct(int index) {
    setState(() {
      _products.removeAt(index);
    });
  }

  @override
  build(context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.deepPurple),
      //home: AuthPage(),
      routes: {
        '/': (BuildContext context) =>
            ProductsPage(_products ),
        '/admin': (BuildContext context) => ProductsAdminPage(_addProduct, _deleteProduct),
      },
      onGenerateRoute: (RouteSettings settings) {
        final List<String> pathElements = settings.name.split('/');
        if (pathElements[0] != '') {
          return null;
        }
        if (pathElements[1] == 'product') {
          final int index = int.parse(pathElements[2]);
          return MaterialPageRoute<bool>(
            builder: (BuildContext context) {
              return ProductPage(
                  _products[index]['title'], _products[index]['image']);
            },
          );
        }
        return null;
      },
      onUnknownRoute: (RouteSettings settings) {
        print('onUnknownRoute -> ' + settings.name.toString());
        return MaterialPageRoute(
          builder: (BuildContext context) =>
              ProductsPage(_products),
        );
      },
    );
  }
}

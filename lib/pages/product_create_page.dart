import 'package:flutter/material.dart';

class ProductCreatePage extends StatefulWidget {
  final Function addProduct;

  ProductCreatePage(this.addProduct);

  @override
  State<StatefulWidget> createState() {
    return _ProductCreateStatePage();
  }
}

class _ProductCreateStatePage extends State<ProductCreatePage> {
  String _titleValue;
  String _description;
  double _priceValue;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: 'Title'),
              onChanged: (String value) {
                setState(() {
                  _titleValue = value;
                });
              },
            ),
            TextField(
              maxLines: 5,
              decoration: InputDecoration(labelText: 'Description'),
              onChanged: (String value) {
                setState(() {
                  _description = value;
                });
              },
            ),
            TextField(
              decoration: InputDecoration(labelText: 'Price'),
              keyboardType: TextInputType.number,
              onChanged: (String value) {
                setState(() {
                  _priceValue = double.tryParse(value);
                });
              },
            ),
            RaisedButton(
              child: Text('Save'),
              color: Theme.of(context).accentColor,
              onPressed: () {
                final Map<String, dynamic> product = {
                  'title': _titleValue,
                  'description': _description,
                  'price': _priceValue,
                  'image': 'assets/images/rocket.png'
                };
                widget.addProduct(product);
                Navigator.pushReplacementNamed(context, '/');
              },
            )
          ],
        ),
      );
}
